from django.urls import path
from .views import AnimalView

urlpatterns = [
    path('animals/', AnimalView.as_view()),
    #NAO ESQUECER de colocar a barra depois
    path('animals/<int:animal_id>/', AnimalView.as_view()),
]