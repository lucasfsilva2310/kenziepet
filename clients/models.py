from django.db import models

# Create your models here.
class Group(models.Model):
    name = models.CharField(max_length=255, unique=True)
    scientific_name = models.CharField(max_length=255)

class Characteristic(models.Model):
    characteristic = models.CharField(max_length=255, unique=True)

class Animal(models.Model):
    name = models.CharField(max_length=255)
    age = models.IntegerField()
    weight = models.FloatField()
    sex = models.CharField(max_length=255)

    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    characteristic_set = models.ManyToManyField(Characteristic)

