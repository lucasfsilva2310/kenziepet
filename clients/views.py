from django.shortcuts import render
from django.shortcuts import get_object_or_404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Group, Animal, Characteristic

from .serializers import GroupSerializer, AnimalSerializer, CharacteristicSerializer


# Create your views here.

class AnimalView(APIView):
    def get(self,request, animal_id=0):
        if animal_id == 0:
            queryset = Animal.objects.all()
            serializer = AnimalSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        
        animal = get_object_or_404(Animal, id=animal_id)
        serializer = AnimalSerializer(animal)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self,request):
        # entrada dos dados
        animal_serializer = AnimalSerializer(data=request.data)

        # validando pra verificar se os dados fazem sentido
        if not animal_serializer.is_valid():
            return Response(animal_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    # primeira parte
        # persistencia dos dados do Animal
        data = animal_serializer.data

        # Separando as características
        characteristic_set = data.pop('characteristic_set')

        # separando o grupo
        group = data.pop('group')

    # segunda parte
        # criando um grupo
        # usar .get_or_create para pegar o objeto (primeiro espaço da tupla)
        new_group = Group.objects.get_or_create(**group)[0]
        
        # Criando um Animal com relação ao grupo
        # Relação 1 -> N vc cria a classe JÁ referenciando o lado 1
        animal = Animal.objects.create(**data, group=new_group)

        # criando cada caracteristica e adicionando ao animal
        # Relação N -> N vc primeiro CRIA a classe, e depois ADICIONA ela a outra classe N
        # Como nessa relação tanto um lado quanto outro podem existir sozinho, vc nao precisa 
        # necessariamente vincular um ao outro de inicio. primeiro vc cria a linha, depois relaciona
        for characteristic in characteristic_set:
            # usar .get_or_create para pegar o objeto (primeiro espaço da tupla)
            charac = Characteristic.objects.get_or_create(**characteristic)[0]
            # charac = Characteristic.objects.create(**characteristic)
            animal.characteristic_set.add(charac)

        # Criando um serializer novo com os dados recebidos
        animal_serializer = AnimalSerializer(animal)

        return Response(animal_serializer.data, status=status.HTTP_201_CREATED)

    def delete(self,request, animal_id):
        animal = get_object_or_404(Animal, id=animal_id)

        animal.delete()
        # pq nao está passando no teste?
        return Response(status=status.HTTP_204_NO_CONTENT)