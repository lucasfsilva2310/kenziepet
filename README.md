# KenziePet

## Descrição
Essa API tem como objetivo funcionar como um sistema de atendimento para um petshop.

## Instalação

As tecnologias necessárias para o funcionamento dessa API são Django, e Django Rest Framework.

Após clonar o repositório, basta rodar um `pip install requirements.txt` para instalar as dependências do projeto.

## Utilização

### GET {URL}/animals

**Response -> HTTP 200**

```[
  {
    "id": 1,
    "name": "Bidu",
    "age": 1.0,
    "weight": 30.0,
    "sex": "macho",
    "group": {
      "id": 1,
      "name": "cao",
      "scientific_name": "canis familiaris"
    },
    "characteristic_set": [
      {
        "id": 1,
        "characteristic": "peludo"
      },
      {
        "id": 2,
        "characteristic": "medio porte"
      }
    ]
  },
  {
    "id": 2,
    "name": "Hanna",
    "age": 1.0,
    "weight": 20.0, 
    "sex": "femea",
    "group": {
      "id": 2,
      "name": "gato",
      "scientific_name": "felis catus"
    },
    "characteristic_set": [
      {
        "id": 1,
        "characteristic": "peludo"
      },
      {
        "id": 3,
        "characteristic": "felino"
      }
    ]
  }
]
```

### GET {URL}/animals/1

**Response -> HTTP 200**

```
{
    "id": 1,
    "name": "Bidu",
    "age": 1.0,
    "weight": 30.0,
    "sex": "macho",
    "group": {
      "id": 1,
      "name": "cao",
      "scientific_name": "canis familiaris"
    },
    "characteristic_set": [
      {
        "id": 1,
        "characteristic": "peludo"
      },
      {
        "id": 2,
        "characteristic": "medio porte"
      }
    ]
  },
```

### POST {URL}/animals
**Request -> HTTP 201**
```
{
  "name": "Bidu",
	"age": 1,
	"weight": 30,
	"sex": "macho",
  "group": {
	"name": "cao",
	"scientific_name": "canis familiaris"
	},
  "characteristic_set": [
    {
	"characteristic": "peludo"
    },
    {
	"characteristic": "medio porte"
    }
  ]
}
```

**Response -> HTTP 201**
```
{
  "id": 1,
  "name": "Bidu",
  "age": 1.0,
  "weight": 30.0,
  "sex": "macho",
  "group": {
    "id": 1,
    "name": "cao",
    "scientific_name": "canis familiaris"
  },
  "characteristic_set": [
    {
      "id": 1,
      "characteristic": "peludo"
    },
    {
      "id": 2,
      "characteristic": "medio porte"
    }
  ]
}
```

### DELETE {URL}/animals/1

**Response -> HTTP 204**

```
no-content
```